import Catalog.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@TestPropertySource(properties = {
        "spring.profiles.active=default"})
public class testForExample {


    @Test
    public void testForExmaple() {
        //test for exmaple
    }

    @Test
    public void testForExmaple2() {
        //another test for exmaple
    }
}
