package Catalog.Exceptions;

public class CategoryError extends RuntimeException {
    public CategoryError(String name, boolean isExist) {

        super("Category with this name "+name+ (isExist?" already":" not")+" exist");
    }
}
