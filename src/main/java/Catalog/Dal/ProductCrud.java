package Catalog.Dal;

import Catalog.Data.Product;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface ProductCrud extends PagingAndSortingRepository <Product,String> {


    List<Product>
    findAllByName(
            @Param("name") String name,
            Pageable pageable);

    List<Product>
    findAllByCategoryName(
            @Param("name") String name,
            Pageable pageable);

    List<Product>
    findAllByPriceGreaterThan(
            @Param("price") double price,
            Pageable pageable);

    List<Product>
    findAllByPriceLessThan(
            @Param("price") double price,
            Pageable pageable);




}
