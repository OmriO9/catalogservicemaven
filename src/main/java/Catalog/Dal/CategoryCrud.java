package Catalog.Dal;

import Catalog.Data.Category;
import org.springframework.data.repository.PagingAndSortingRepository;



public interface CategoryCrud extends PagingAndSortingRepository <Category,String> {

}
