package Catalog.Layout;

import Catalog.Exceptions.CategoryError;
import Catalog.Exceptions.FieldException;
import Catalog.Exceptions.ProductNotFoundException;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Collections;
import java.util.Map;

@ControllerAdvice
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class ExceptionHandlerController {

    private static final String BASIC_CATALOG_ROUTE = "/catalog";


    public ExceptionHandlerController() {
    }


    @ExceptionHandler(FieldException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ResponseEntity<Map<String,String>> handleArgumentNotValid (FieldException e){
        String message = e.getMessage();
        if (message == null) {
            message = "Arguments are not valid";
        }
        return new ResponseEntity<>(Collections.singletonMap("Error", message),HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ProductNotFoundException.class)
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    public ResponseEntity<Map<String,String>> handleCustomerNotFound(ProductNotFoundException e) {
        String message = e.getMessage();
        if (message == null) {
            message = "Product Not Found";
        }
        return new ResponseEntity<>(Collections.singletonMap("Error", message),HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(PropertyReferenceException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ResponseEntity<Map<String,String>> handleSortAttrNotFound(PropertyReferenceException e) {
        String message = "Error with the Sorting attribute you entered ";
        return new ResponseEntity<>(Collections.singletonMap("Error", message), HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<Map<String,String>> handleCategoryExistException(CategoryError e){
        String message = e.getMessage();
        if (message == null) {
            message = "There is an error with the category you entered";
        }
        return new ResponseEntity<>(Collections.singletonMap("Error", message),HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ConversionFailedException.class)
    @ResponseStatus(code=HttpStatus.BAD_REQUEST)
    public ResponseEntity<Map<String,String>> handleMethodArgumentTypeMismatchException(ConversionFailedException e) {
        Class<?> type = e.getTargetType().getType();
        String message;
        if(type.isEnum()){
            message = "This isn't a valid filtering option";
        }
        else{
            message = "error with one of the request params";
        }
        return new ResponseEntity<>(Collections.singletonMap("Error", message),HttpStatus.BAD_REQUEST);
    }




}
