package Catalog.Layout;

import Catalog.Layout.Boundaries.CategoryBoundary;
import Catalog.logic.Interfaces.ICategoryService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class CategoryController{
    private static final String BASIC_CATALOG_ROUTE = "/catalog";
    private static final String BASIC_CATEGORY_ROUTE = BASIC_CATALOG_ROUTE + "/categories";
    private static final String BASIC_PRODUCT_ROUTE = BASIC_CATALOG_ROUTE + "/products";

    private ICategoryService categoryService;

    public CategoryController(ICategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @RequestMapping(path = BASIC_CATEGORY_ROUTE,
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public CategoryBoundary createCategory(@RequestBody CategoryBoundary newCategory) {
        return new CategoryBoundary(this.categoryService.validateBoundary(newCategory)
                .createCategory(newCategory.convertToEntity()));
    }


    @RequestMapping(path = BASIC_CATEGORY_ROUTE, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CategoryBoundary[] getAllCategories(
            @RequestParam(name = "sortBy", required = false, defaultValue = "name") String sortBy,
            @RequestParam(name = "size", required = false, defaultValue = "20") int size,
            @RequestParam(name = "page", required = false, defaultValue = "0") int page) {
        return this.categoryService.getCategories(sortBy, size, page)
                .stream()
                .map(CategoryBoundary::new)
                .toArray(CategoryBoundary[]::new);
    }

    @RequestMapping(path = BASIC_CATALOG_ROUTE,
            method = RequestMethod.DELETE)
    public void deleteCatalog() {
        this.categoryService.deleteAll();
    }


}
