package Catalog.Layout;


import Catalog.Data.FilterTypes;
import Catalog.Layout.Boundaries.ProductBoundray;
import Catalog.logic.Interfaces.IProductService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


@RestController
public class ProductController {
    private static final String BASIC_CATALOG_ROUTE = "/catalog";
    private static final String BASIC_CATEGORY_ROUTE = BASIC_CATALOG_ROUTE + "/categories";
    private static final String BASIC_PRODUCT_ROUTE = BASIC_CATALOG_ROUTE + "/products";

    private IProductService productService;


    public ProductController(IProductService productService) {
        this.productService = productService;
    }

    @RequestMapping(path = BASIC_PRODUCT_ROUTE,
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ProductBoundray createProduct(@RequestBody ProductBoundray newProduct) {
        return new ProductBoundray(this.productService.validateBoundary(newProduct)
                .createProduct(newProduct.convertToEntity()));
    }

    @RequestMapping(path = BASIC_PRODUCT_ROUTE + "/{productId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ProductBoundray getProduct(
            @PathVariable("productId") String productId) {
        return new ProductBoundray(this.productService.getProduct(productId));
    }

    @RequestMapping(path = BASIC_PRODUCT_ROUTE,
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ProductBoundray[] getProducts(
            @RequestParam(name = "filterType", required = false, defaultValue = "all") FilterTypes filterType,
            @RequestParam(name = "filterValue", required = false, defaultValue = "") String filterValue,
            @RequestParam(name = "sortBy", required = false, defaultValue = "name") String sortBy,
            @RequestParam(name = "size", required = false, defaultValue = "20") int size,
            @RequestParam(name = "page", required = false, defaultValue = "0") int page) {
        return this.productService.validateFilterArgs(filterType, filterValue, sortBy)
                .getProductsByFilter(filterType, size, page, filterValue, sortBy)
                .stream()
                .map(ProductBoundray::new)
                .toArray(ProductBoundray[]::new);
    }
}
