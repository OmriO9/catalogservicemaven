package Catalog.Layout.Boundaries;

import Catalog.Data.Product;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductBoundray {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    private String name;
    private Double price;
    private String image;
    private CategoryBoundary category;

    public ProductBoundray(String name, double price, String image, CategoryBoundary category) {
        this.name = name;
        this.price = price;
        this.image = image;
        this.category = category;
    }

    public ProductBoundray() {
    }

    public ProductBoundray(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.image = product.getImage();
        this.price = product.getPrice();
        this.category = new CategoryBoundary(product.getCategory());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public CategoryBoundary getCategory() {
        return category;
    }

    public void setCategory(CategoryBoundary category) {
        this.category = category;
    }

    public Product convertToEntity(){
        Product newProduct= new Product();
        newProduct.setImage(this.image);
        newProduct.setName(this.name);
        newProduct.setPrice(this.price);
        newProduct.setCategory(this.category.convertToEntity());
        return newProduct;
    }

}
