package Catalog.Layout.Boundaries;

import Catalog.Data.Category;

public class CategoryBoundary {
    private String name;
    private String description;

    public CategoryBoundary() {
    }

    public CategoryBoundary(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public CategoryBoundary(Category category) {
        this.name = category.getName();
        this.description = category.getDescription();
    }

    public Category convertToEntity() {
        Category newCategory = new Category();
        newCategory.setName(this.name);
        newCategory.setDescription(this.description);
        return newCategory;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
