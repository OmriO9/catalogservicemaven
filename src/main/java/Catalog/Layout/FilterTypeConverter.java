package Catalog.Layout;

import Catalog.Data.FilterTypes;
import Catalog.Exceptions.CommonErrors;
import Catalog.Exceptions.FieldException;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class FilterTypeConverter implements Converter<String, FilterTypes> {
    @Override
    public FilterTypes convert(String source) {
        FilterTypes type= FilterTypes.getByStr(source);
        if(type != null)
            return type;
        throw new FieldException(CommonErrors.NULL_FILTER_TYPE_ERR);
    }
}
