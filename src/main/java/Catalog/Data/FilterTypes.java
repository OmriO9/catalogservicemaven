package Catalog.Data;


public enum FilterTypes {
    BY_MIN_PRICE("byMinPrice"),
    BY_MAX_PRICE("byMaxPrice"),
    BY_NAME("byName"),
    BY_CAT_NAME("byCategoryName"),
    ALL("all");

    private String typeString;

    FilterTypes(String typeString){
        this.typeString = typeString;
    }

    public static FilterTypes getByStr(String name){
        for (FilterTypes filter: FilterTypes.values()) {
            if(filter.typeString.equals(name))
                return filter;
        }
        return null;
    }
}
