package Catalog.logic.Interfaces;

import Catalog.Data.FilterTypes;
import Catalog.Data.Product;
import Catalog.Layout.Boundaries.ProductBoundray;

import java.util.List;

public interface IProductService {
    Product createProduct(Product product);

    Product getProduct(String productId);
    List<Product> getProductsByFilter(FilterTypes filterType, int size, int page, String filterValue, String sortAttr);
    IProductService validateBoundary(ProductBoundray productBoundray);
    IProductService validateFilterArgs(FilterTypes filterType, String filterValue, String sortAttr);
}