package Catalog.logic.Interfaces;

import Catalog.Data.Category;
import Catalog.Layout.Boundaries.CategoryBoundary;

import java.util.List;

public interface ICategoryService {
    Category createCategory (Category category);
    List<Category> getCategories(String sortBy, int size, int page);
    ICategoryService validateBoundary(CategoryBoundary categoryBoundary);
    void deleteAll();
}
