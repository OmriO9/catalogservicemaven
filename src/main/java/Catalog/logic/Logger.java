package Catalog.logic;

import io.sentry.Sentry;
import io.sentry.event.Event;
import io.sentry.event.EventBuilder;
import io.sentry.event.interfaces.ExceptionInterface;
import org.springframework.stereotype.Component;

@Component
public class Logger {
    private static final String SENTRY_DSN = "https://fde83fb0d19446ae824f7407bbc58dd7@sentry.io/1887365";

    public Logger() {
        Sentry.init(SENTRY_DSN);
    }

    public void send(String message,String className,Event.Level level){
        Sentry.capture(new EventBuilder()
                .withMessage(message)
                .withLevel(level)
                .withLogger(className));
    }


    public RuntimeException sendExecption(String message,String className,Event.Level level,RuntimeException e){
        Sentry.capture(new EventBuilder()
                .withMessage(message)
                .withLevel(level)
                .withLogger(className)
                .withSentryInterface(new ExceptionInterface(e)));
        return e;

    }




}
