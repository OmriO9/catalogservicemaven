package Catalog.logic;

import Catalog.Dal.CategoryCrud;
import Catalog.Dal.ProductCrud;
import Catalog.Data.Category;
import Catalog.Exceptions.CategoryError;
import Catalog.Exceptions.CommonErrors;
import Catalog.Exceptions.FieldException;
import Catalog.Layout.Boundaries.CategoryBoundary;
import Catalog.logic.Interfaces.ICategoryService;
import io.sentry.event.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService implements ICategoryService {

    private CategoryCrud categoryCrud;
    private ProductCrud productCrud;
    private Logger logger;
    private Validator validator;

    @Autowired
    public CategoryService(CategoryCrud categoryCrud, Validator validator, ProductCrud productCrud,
                           Logger logger) {
        this.categoryCrud = categoryCrud;
        this.validator = validator;
        this.productCrud = productCrud;
        this.logger = logger;
    }

    @Override
    public Category createCategory(Category category) {

        this.logger.send("new category Created! : " + category.getName(),
                this.getClass().getSimpleName(),
                Event.Level.INFO);

        return this.categoryCrud.save(category);
    }

    @Override
    public List<Category> getCategories(String sortBy, int size, int page) {

        this.logger.send("get all categories by " + sortBy,
                this.getClass().getSimpleName(),
                Event.Level.INFO);

        return this.categoryCrud.findAll(PageRequest.of(page, size,Sort.by(sortBy))).getContent();
    }

    @Override
    public CategoryService validateBoundary(CategoryBoundary categoryBoundary) {

        if(validator.validate(categoryBoundary)) {
            if(this.categoryCrud.existsById(categoryBoundary.getName()))

                throw this.logger.sendExecption("Exception in validate category boundary",
                        this.getClass().getSimpleName(),Event.Level.ERROR, new CategoryError(categoryBoundary.getName(),true));

            return this;
        }

        throw this.logger.sendExecption("Exception in validate category boundary",
                this.getClass().getSimpleName(),Event.Level.ERROR,  new FieldException(CommonErrors.CATEGORY_DETAILS_ERR));
    }



    @Override
    public void deleteAll() {
        this.categoryCrud.deleteAll();
        this.productCrud.deleteAll();

        this.logger.send("All the categories and products deleted!",
                this.getClass().getSimpleName(),Event.Level.WARNING);

    }
}
