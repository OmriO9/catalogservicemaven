package Catalog.logic;

import Catalog.Dal.CategoryCrud;
import Catalog.Dal.ProductCrud;
import Catalog.Data.FilterTypes;
import Catalog.Data.Product;
import Catalog.Exceptions.CategoryError;
import Catalog.Exceptions.CommonErrors;
import Catalog.Exceptions.FieldException;
import Catalog.Exceptions.ProductNotFoundException;
import Catalog.Layout.Boundaries.ProductBoundray;
import Catalog.logic.Interfaces.IProductService;
import io.sentry.event.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService implements IProductService {
    private ProductCrud productCrud;
    private Validator validator;
    private CategoryCrud categoryCrud;
    private Logger logger;

    @Autowired
    public ProductService(ProductCrud productCrud, Validator validator, CategoryCrud categoryCrud, Logger logger) {
        this.productCrud = productCrud;
        this.validator = validator;
        this.categoryCrud = categoryCrud;
        this.logger = logger;
    }

    @Override
    public Product createProduct(Product product) {
        product.setCategory(categoryCrud.findById(product.getCategory().getName()).get());

        this.logger.send("new product Created! : " + product.getName(),
                this.getClass().getSimpleName(), Event.Level.INFO);

        return productCrud.save(product);
    }

    @Override
    public Product getProduct(String productId) {
        if (!validator.validate(productId))
            throw this.logger.sendExecption("Error in get product",
                    this.getClass().getSimpleName(),
                    Event.Level.ERROR,
                    new FieldException(CommonErrors.PRODUCT_ID_ERR));
        return this.productCrud.findById(productId)
                .orElseThrow(() -> new ProductNotFoundException(productId));
    }

    public List<Product> getProductsByFilter(FilterTypes filterType, int size, int page, String filterValue, String sortAttr) {
        switch (filterType) {
            case ALL:
                return this.getAllProducts(size, page, sortAttr);
            case BY_NAME:
                return this.getAllProductsByProductName(size, page, filterValue, sortAttr);
            case BY_MAX_PRICE:
                return this.getAllProductsByMaxPrice(size, page, sortAttr, getPriceFromString(filterValue));
            case BY_MIN_PRICE:
                return this.getAllProductsByMinPrice(size, page, sortAttr, getPriceFromString(filterValue));
            case BY_CAT_NAME:
                return this.getAllProductsByCategoryName(size, page, filterValue, sortAttr);
            default:

                throw this.logger.sendExecption("Error in getProductsByFilter",
                        this.getClass().getSimpleName(),
                        Event.Level.ERROR,
                        new FieldException(CommonErrors.NULL_FILTER_TYPE_ERR));
        }
    }

    private List<Product> getAllProducts(int size, int page, String sortAttr) {
        return this.productCrud.findAll(PageRequest.of(page, size, Sort.by(sortAttr))).getContent();
    }


    private List<Product> getAllProductsByProductName(int size, int page, String name, String sortAttr) {
        return this.productCrud.findAllByName(name, PageRequest.of(page, size, Sort.by(sortAttr)));
    }

    private List<Product> getAllProductsByMinPrice(int size, int page, String sortAttr, double price) {
        return this.productCrud.findAllByPriceGreaterThan(price, PageRequest.of(page, size, Sort.by(sortAttr)));
    }

    private List<Product> getAllProductsByMaxPrice(int size, int page, String sortAttr, double price) {
        return this.productCrud.findAllByPriceLessThan(price, PageRequest.of(page, size, Sort.by(sortAttr)));
    }

    private List<Product> getAllProductsByCategoryName(int size, int page, String name, String sortAttr) {
        return this.productCrud.findAllByCategoryName(name, PageRequest.of(page, size, Sort.by(sortAttr)));
    }

    @Override
    public ProductService validateFilterArgs(FilterTypes filterType, String filterValue, String sortAttr) {
        boolean allFieldsValid = validator.validate(filterType)
                && validator.validate(sortAttr);
        if (allFieldsValid) {
            boolean isFilterValueValid = validator.validate(filterValue);
            if (!filterType.equals(FilterTypes.ALL) && !isFilterValueValid)

                throw this.logger.sendExecption("Error in validateFilterArgs",
                        this.getClass().getSimpleName(),
                        Event.Level.ERROR,
                        new FieldException(CommonErrors.FILTER_VALUE_ERR));

            return this;
        }

        throw this.logger.sendExecption("Error in validateFilterArgs",
                this.getClass().getSimpleName(),
                Event.Level.ERROR,
                new FieldException(CommonErrors.FILTER_ARGS_ERR));
    }

    @Override
    public ProductService validateBoundary(ProductBoundray productBoundray) {
        if (validator.validate(productBoundray)) {
            if (categoryCrud.existsById(productBoundray.getCategory().getName()))
                return this;
            else
                throw this.logger.sendExecption("Error in validateBoundary, product Service",
                        this.getClass().getSimpleName(),
                        Event.Level.ERROR,
                        new CategoryError(productBoundray.getCategory().getName(), false));
        }

        throw this.logger.sendExecption("Error in validateBoundary, product Service",
                this.getClass().getSimpleName(),
                Event.Level.ERROR,
                new FieldException(CommonErrors.PRODUCT_DETAILS_ERR));
    }

    private Double getPriceFromString(String price) {
        try {
            Double maxPrice = Double.valueOf(price);
            return maxPrice;
        } catch (NumberFormatException ex) {

            throw this.logger.sendExecption("Error in try to get price from given price",
                    this.getClass().getSimpleName(),
                    Event.Level.ERROR,
                    new FieldException(CommonErrors.PRICE_IS_NOT_NUMBER_ERR));
        }
    }

}
