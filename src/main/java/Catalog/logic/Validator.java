package Catalog.logic;

import Catalog.Data.FilterTypes;
import Catalog.Layout.Boundaries.CategoryBoundary;
import Catalog.Layout.Boundaries.ProductBoundray;
import org.springframework.stereotype.Component;

@Component
public class Validator {
    public Validator() {
    }

    public boolean validate(String str) {
        if (str != null)
            return !str.trim().isEmpty();
        return false;
    }

    public boolean validate(Double price) {
        return price != null && price >= 0;
    }

    public boolean validate(FilterTypes filterType) {
        return filterType != null;
    }

    public boolean validate(ProductBoundray productBoundray) {
       return this.validate(productBoundray.getImage())
                && this.validate(productBoundray.getName())
                && this.validate(productBoundray.getPrice())
                && this.validate(productBoundray.getCategory());
    }

    public boolean validate(CategoryBoundary categoryBoundary) {
        if (categoryBoundary != null)
            return this.validate(categoryBoundary.getName())
                    && this.validate(categoryBoundary.getDescription());
        return false;
    }

}
